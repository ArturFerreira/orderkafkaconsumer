package org.artur.serdes;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.artur.domain.dto.PedidoItemDto;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

public class PedidoSerdes extends Serdes.WrapperSerde<PedidoItemDto> {

    public PedidoSerdes(){
        super(new JsonSerializer<>(), new JsonDeserializer<>());
    }

    public static Serde<PedidoItemDto> serdes(){
        JsonSerializer<PedidoItemDto> serializer = new JsonSerializer<>();
        JsonDeserializer<PedidoItemDto> deserializer = new JsonDeserializer<>(PedidoItemDto.class);

        return Serdes.serdeFrom(serializer, deserializer);
    }
}
