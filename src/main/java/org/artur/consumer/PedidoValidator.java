package org.artur.consumer;

import org.artur.avro.PedidoRecord;
import org.artur.domain.enumeration.StatusProcessamento;
import org.artur.domain.enumeration.StatusTracking;
import org.artur.domain.model.Pedido;
import org.artur.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PedidoValidator {
    @Autowired
    private PedidoRepository pedidoRepository;

    @KafkaListener(topics = "pedido-topic" , groupId = "group_id")
    public void processarPedido(PedidoRecord pedidoRecord){
        Pedido pedido = pedidoRepository.findByIdentificador(pedidoRecord.getIdentificador().toString());

        if(pedido != null){
            pedido.setStatusProcessamento(StatusProcessamento.PROCESSADO);
            pedido.setStatusTracking(StatusTracking.MAP);
        }

        pedidoRepository.save(pedido);
    }
}
