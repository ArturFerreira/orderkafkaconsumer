package org.artur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderKafkaConsumerApiAplication {
    public static void main(String[] args){
        SpringApplication.run(OrderKafkaConsumerApiAplication.class, args);
    }
}