package org.artur.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemDto {
    private int idProduto;
    private String descricao;
    private int qtd;
    private double preco;
}
