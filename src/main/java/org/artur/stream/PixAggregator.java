package org.artur.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.artur.serdes.PedidoSerdes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PixAggregator
{
    @Autowired
    public void aggregator(StreamsBuilder streamBuilder){
        KTable<String, Double> stringPedidoDTOKStream = streamBuilder
                .stream("pedido-topic", Consumed.with(Serdes.String(), PedidoSerdes.serdes()))
                .peek((key, value) -> System.out.println("Pedido recebido de: " +value.getIdentificador()))
                .groupBy((key, value) -> value.getIdentificador())
                .aggregate(
                        () -> 0.0,
                        (key,value,aggregate) -> (aggregate + value.getTotalPedido()),
                        Materialized.with(Serdes.String(), Serdes.Double())
                );

        stringPedidoDTOKStream.toStream().print(Printed.toSysOut());
        stringPedidoDTOKStream.toStream().to("pedido-topic-verificacao-fraude", Produced.with(Serdes.String(), Serdes.Double()));
    }
}
