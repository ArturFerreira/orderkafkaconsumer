# Order Kafka Consumer


<h4 align="center"> 
	🚧  Status do projeto 🚀 Em construção...  🚧
</h4>

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Java]()
- [Spring](https://spring.io/)
- [Sql Server]()
- [Apache Kafka]()

### Autor
---

<a href="https://gitlab.com/ArturFerreira/orderkafkaconsumer">

<sub><b>Artur de Oliveira Ferreira</b></sub></a>